#!/bin/bash

if [[ -z $ANACONDA_TOKEN || \
      -z $1 ]] ; then
  echo "Required environment variables have not been set!"
  exit -1
fi

mkdir -p conda-bld/linux-64 conda-bld/noarch

# 172800 seconds is 2 days
gitlab-runner exec docker \
    --env HOST_USER_ID="$(id -u $USER)" \
    --env ANACONDA_TOKEN="${ANACONDA_TOKEN}" \
    --env CI_PROJECT_NAME="$(basename $(pwd))" \
    --env CI_PROJECT_NAMESPACE="$(basename $(dirname $(pwd)))" \
    --docker-volumes `pwd`/conda-bld/linux-64:/opt/conda/conda-bld/linux-64 \
    --timeout 172800 \
    $1
